﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GAC.GAC;
using GAC.GAC.GACDataTableAdapters;
using GAC.ADSync;
using GAC.GoogleSync;

namespace GACSync
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("GACSync");
        private static readonly log4net.ILog logEmail = log4net.LogManager.GetLogger("GACSyncEmail");

        static GACTool gac;
        static ADSync ad;
        static GoogleSync google;

        static bool inProgress = false;
        static bool errorOccured = false;


        static void Main(string[] args)
        {
            StatusReport("GACSync", "Starting New Sync");
            StatusReport("GACSync", "-----------------");

            StartGACSync();

            StatusReport("GACSync", "-----------------");
            StatusReport("GACSync", "Sync End");
        }

        static void StartGACSync()
        {
            if (!inProgress)
            {
                inProgress = true;
                try
                {
                    gac = new GACTool();
                    gac.StatusEvent += StatusReport;
                    ad = new ADSync();
                    ad.StatusEvent += StatusReport;
                    google = new GoogleSync();
                    google.StatusEvent += StatusReport;

                    DataSync();
                    GACMatch();
                }
                catch (Exception ex)
                {
                    StatusReport("GACSync", "An Error Occured in the GAC Sync Process", true);
                    StatusReport("GACSync", ex.StackTrace.ToString(), true);
                }
                inProgress = false;
            }
        }

        static void DataSync()
        {
            List<Task> tasks = new List<Task>();
            QueriesTableAdapter Q = new QueriesTableAdapter();

            tasks.Add(Task.Run(() =>
            {
                google.GetGooogleData();
                Q.SyncGoogleUsers();
            }));
            tasks.Add(Task.Run(() =>
            {
                ad.GetADData();
            }));
            tasks.Add(Task.Run(() =>
            {
                Q.SyncChris21Users();
            }));

            Task.WaitAll(tasks.ToArray());

            if (!errorOccured)
            {
                Q.GACSyncSuccess();
            }
        }

        static void GACMatch()
        {
            gac.StartMatchProccess();
            gac.WriteIdentityData();
        }

        static void StatusReport(string Location, string Status, bool error = false)
        {
            string msg = $"{Location}: {Status}";
            Console.WriteLine(msg);
            if (!error)
            {
                log.Info(msg);
            }
            else
            {
                errorOccured = true;
                log.Error(msg);
                logEmail.Error(msg);
            }
        }
    }
}
