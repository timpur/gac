﻿namespace GAC.GAC
{
    partial class GACForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GACForm));
            this.ConsoleTextBox = new System.Windows.Forms.TextBox();
            this.MatchBTN = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.Debug = new System.Windows.Forms.ComboBox();
            this.SaveMatchBTN = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.SyncAllBTN = new System.Windows.Forms.Button();
            this.checkBoxExit2 = new System.Windows.Forms.CheckBox();
            this.checkBoxExit1 = new System.Windows.Forms.CheckBox();
            this.Chris21DataBTN = new System.Windows.Forms.Button();
            this.GoogleSyncCB = new System.Windows.Forms.CheckBox();
            this.ADDataBTN = new System.Windows.Forms.Button();
            this.GoogleDataBTN = new System.Windows.Forms.Button();
            this.ReportSaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ConsoleTextBox
            // 
            this.ConsoleTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ConsoleTextBox.Location = new System.Drawing.Point(3, 3);
            this.ConsoleTextBox.Margin = new System.Windows.Forms.Padding(10);
            this.ConsoleTextBox.Multiline = true;
            this.ConsoleTextBox.Name = "ConsoleTextBox";
            this.ConsoleTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.ConsoleTextBox.Size = new System.Drawing.Size(420, 329);
            this.ConsoleTextBox.TabIndex = 0;
            this.ConsoleTextBox.WordWrap = false;
            // 
            // MatchBTN
            // 
            this.MatchBTN.Location = new System.Drawing.Point(130, 0);
            this.MatchBTN.Name = "MatchBTN";
            this.MatchBTN.Size = new System.Drawing.Size(115, 51);
            this.MatchBTN.TabIndex = 1;
            this.MatchBTN.Text = "Match Entities";
            this.MatchBTN.UseVisualStyleBackColor = true;
            this.MatchBTN.Click += new System.EventHandler(this.MatchBTN_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(434, 412);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.ConsoleTextBox);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(426, 386);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Main";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.Debug);
            this.panel1.Controls.Add(this.SaveMatchBTN);
            this.panel1.Controls.Add(this.MatchBTN);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(3, 332);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(420, 51);
            this.panel1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Debug Level";
            // 
            // Debug
            // 
            this.Debug.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Debug.FormattingEnabled = true;
            this.Debug.Items.AddRange(new object[] {
            "None",
            "Limited",
            "All"});
            this.Debug.Location = new System.Drawing.Point(3, 25);
            this.Debug.Name = "Debug";
            this.Debug.Size = new System.Drawing.Size(121, 21);
            this.Debug.TabIndex = 3;
            // 
            // SaveMatchBTN
            // 
            this.SaveMatchBTN.Dock = System.Windows.Forms.DockStyle.Right;
            this.SaveMatchBTN.Location = new System.Drawing.Point(312, 0);
            this.SaveMatchBTN.Name = "SaveMatchBTN";
            this.SaveMatchBTN.Size = new System.Drawing.Size(108, 51);
            this.SaveMatchBTN.TabIndex = 2;
            this.SaveMatchBTN.Text = "Save Match Data";
            this.SaveMatchBTN.UseVisualStyleBackColor = true;
            this.SaveMatchBTN.Click += new System.EventHandler(this.SaveMatchBTN_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.SyncAllBTN);
            this.tabPage2.Controls.Add(this.checkBoxExit2);
            this.tabPage2.Controls.Add(this.checkBoxExit1);
            this.tabPage2.Controls.Add(this.Chris21DataBTN);
            this.tabPage2.Controls.Add(this.GoogleSyncCB);
            this.tabPage2.Controls.Add(this.ADDataBTN);
            this.tabPage2.Controls.Add(this.GoogleDataBTN);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(426, 386);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Load Data";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // SyncAllBTN
            // 
            this.SyncAllBTN.Location = new System.Drawing.Point(8, 163);
            this.SyncAllBTN.Name = "SyncAllBTN";
            this.SyncAllBTN.Size = new System.Drawing.Size(410, 23);
            this.SyncAllBTN.TabIndex = 7;
            this.SyncAllBTN.Text = "Sync All";
            this.SyncAllBTN.UseVisualStyleBackColor = true;
            this.SyncAllBTN.Click += new System.EventHandler(this.SyncAllBTN_Click);
            // 
            // checkBoxExit2
            // 
            this.checkBoxExit2.AutoSize = true;
            this.checkBoxExit2.Checked = true;
            this.checkBoxExit2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxExit2.Location = new System.Drawing.Point(177, 75);
            this.checkBoxExit2.Name = "checkBoxExit2";
            this.checkBoxExit2.Size = new System.Drawing.Size(104, 17);
            this.checkBoxExit2.TabIndex = 6;
            this.checkBoxExit2.Text = "Exit When Done";
            this.checkBoxExit2.UseVisualStyleBackColor = true;
            // 
            // checkBoxExit1
            // 
            this.checkBoxExit1.AutoSize = true;
            this.checkBoxExit1.Checked = true;
            this.checkBoxExit1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxExit1.Location = new System.Drawing.Point(177, 44);
            this.checkBoxExit1.Name = "checkBoxExit1";
            this.checkBoxExit1.Size = new System.Drawing.Size(104, 17);
            this.checkBoxExit1.TabIndex = 5;
            this.checkBoxExit1.Text = "Exit When Done";
            this.checkBoxExit1.UseVisualStyleBackColor = true;
            // 
            // Chris21DataBTN
            // 
            this.Chris21DataBTN.Location = new System.Drawing.Point(8, 121);
            this.Chris21DataBTN.Name = "Chris21DataBTN";
            this.Chris21DataBTN.Size = new System.Drawing.Size(163, 23);
            this.Chris21DataBTN.TabIndex = 4;
            this.Chris21DataBTN.Text = "Sync Chris 21 Data To GAC";
            this.Chris21DataBTN.UseVisualStyleBackColor = true;
            this.Chris21DataBTN.Click += new System.EventHandler(this.Chris21DataBTN_Click);
            // 
            // GoogleSyncCB
            // 
            this.GoogleSyncCB.AutoSize = true;
            this.GoogleSyncCB.Checked = true;
            this.GoogleSyncCB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.GoogleSyncCB.Location = new System.Drawing.Point(177, 21);
            this.GoogleSyncCB.Name = "GoogleSyncCB";
            this.GoogleSyncCB.Size = new System.Drawing.Size(154, 17);
            this.GoogleSyncCB.TabIndex = 3;
            this.GoogleSyncCB.Text = "Sync Google Data To GAC";
            this.GoogleSyncCB.UseVisualStyleBackColor = true;
            // 
            // ADDataBTN
            // 
            this.ADDataBTN.Location = new System.Drawing.Point(8, 69);
            this.ADDataBTN.Name = "ADDataBTN";
            this.ADDataBTN.Size = new System.Drawing.Size(163, 23);
            this.ADDataBTN.TabIndex = 1;
            this.ADDataBTN.Text = "Open AD Data Sync To GAC";
            this.ADDataBTN.UseVisualStyleBackColor = true;
            this.ADDataBTN.Click += new System.EventHandler(this.ADDataBTN_Click);
            // 
            // GoogleDataBTN
            // 
            this.GoogleDataBTN.Location = new System.Drawing.Point(8, 17);
            this.GoogleDataBTN.Name = "GoogleDataBTN";
            this.GoogleDataBTN.Size = new System.Drawing.Size(163, 23);
            this.GoogleDataBTN.TabIndex = 0;
            this.GoogleDataBTN.Text = "Open Google Data Sync";
            this.GoogleDataBTN.UseVisualStyleBackColor = true;
            this.GoogleDataBTN.Click += new System.EventHandler(this.GoogleDataBTN_Click);
            // 
            // GACForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 412);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GACForm";
            this.Text = "GAC Audit";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox ConsoleTextBox;
        private System.Windows.Forms.Button MatchBTN;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.SaveFileDialog ReportSaveFileDialog;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button SaveMatchBTN;
        private System.Windows.Forms.Button Chris21DataBTN;
        private System.Windows.Forms.CheckBox GoogleSyncCB;
        private System.Windows.Forms.Button ADDataBTN;
        private System.Windows.Forms.Button GoogleDataBTN;
        private System.Windows.Forms.CheckBox checkBoxExit2;
        private System.Windows.Forms.CheckBox checkBoxExit1;
        private System.Windows.Forms.ComboBox Debug;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button SyncAllBTN;

    }
}

