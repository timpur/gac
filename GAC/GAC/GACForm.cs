﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GAC.GAC
{
    public partial class GACForm : Form
    {
        GACTool gac;
        public GACForm()
        {
            InitializeComponent();
            Debug.SelectedIndex = 1;

            gac = new GACTool();
            gac.StatusEvent += gac_DebugReport;
        }

        void gac_DebugReport(string Location, string Status, bool error = false)
        {
            if (ConsoleTextBox.InvokeRequired)
            {
                ConsoleTextBox.Invoke(new Action(() => gac_DebugReport(Location, Status)));
            }
            else
            {
                ConsoleTextBox.AppendText(Environment.NewLine + Status + Environment.NewLine);
                ConsoleTextBox.ScrollToCaret();
            }
        }

        private void MatchBTN_Click(object sender, EventArgs e)
        {
            int DebugLevel = Debug.SelectedIndex;
            gac.StatusLevel = DebugLevel;

            System.Threading.Thread T = new System.Threading.Thread(gac.StartMatchProccess);
            T.IsBackground = true;
            T.Start();
            MatchBTN.Enabled = false;
        }

        private void SaveMatchBTN_Click(object sender, EventArgs e)
        {
            System.Threading.Thread T = new System.Threading.Thread(gac.WriteIdentityData);
            T.IsBackground = true;
            T.Start();
            SaveMatchBTN.Enabled = false;
        }

        GoogleSync.GoogleForm GoogleSyncForm;
        private void GoogleDataBTN_Click(object sender, EventArgs e)
        {
            GoogleSyncForm = new GoogleSync.GoogleForm();

            GoogleSyncForm.StartPosition = FormStartPosition.Manual;
            GoogleSyncForm.DesktopLocation = getFormDesktopLocation();
            GoogleSyncForm.done += GoogleSyncForm_done;

            GoogleSyncForm.Show();

            GoogleDataBTN.Enabled = false;
        }
        void GoogleSyncForm_done()
        {
            if (GoogleSyncCB.Checked)
            {
                new GACDataTableAdapters.QueriesTableAdapter().SyncGoogleUsers();
                GoogleSyncCB.Invoke(new Action(() => { GoogleSyncCB.Checked = false; }));
            }

            if (checkBoxExit1.Checked)
            {
                Invoke(new Action(() => GoogleSyncForm.Close()));
                GoogleSyncForm = null;
                GC.Collect();
            }
        }

        ADSync.ADFrom ADSyncForm;
        private void ADDataBTN_Click(object sender, EventArgs e)
        {
            ADSyncForm = new ADSync.ADFrom();

            ADSyncForm.StartPosition = FormStartPosition.Manual;
            ADSyncForm.DesktopLocation = getFormDesktopLocation();
            ADSyncForm.done += ADSyncForm_done;

            ADSyncForm.Show();

            ADDataBTN.Enabled = false;
        }
        void ADSyncForm_done()
        {
            if (checkBoxExit2.Checked)
            {
                Invoke(new Action(() => ADSyncForm.Close()));
                ADSyncForm = null;
                GC.Collect();
            }
        }

        private void Chris21DataBTN_Click(object sender, EventArgs e)
        {
            new GACDataTableAdapters.QueriesTableAdapter().SyncChris21Users();
            Chris21DataBTN.Enabled = false;
        }

        private Point getFormDesktopLocation()
        {
            if ((GoogleSyncForm != null && GoogleSyncForm.Visible) || (ADSyncForm != null && ADSyncForm.Visible))
            {
                return new Point((DesktopLocation.X + Width + 10), (DesktopLocation.Y + 310));
            }
            else
            {
                return new Point((DesktopLocation.X + Width + 10), (DesktopLocation.Y));
            }
        }

        private void SyncAllBTN_Click(object sender, EventArgs e)
        {
            GoogleDataBTN.PerformClick();
            Button GoogleStartBTN = findControlInForm<Button>(GoogleSyncForm, "StartProccessBTN");
            GoogleStartBTN.PerformClick();
            ADDataBTN.PerformClick();
            Button ADStartBTN = findControlInForm<Button>(ADSyncForm, "SyncBTN");
            ADStartBTN.PerformClick();
            Chris21DataBTN.PerformClick();
            SyncAllBTN.Enabled = false;
        }

        private T findControlInForm<T>(Form form, string Name) where T : class
        {
            foreach (Control control in form.Controls)
            {
                if (control.Name == Name)
                    return control as T;
            }
            return null;
        }
    }
}
