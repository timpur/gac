﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel;

namespace GAC.GAC
{
    public delegate void StatusEvent(string Location, string Status, bool error);
    public class GACTool
    {
        public event StatusEvent StatusEvent;
        public int StatusLevel = 1; //0 = None, 1 = Limited, 2 = All

        List<Account> ChrisAccounts = new List<Account>();
        List<Account> ADAccounts = new List<Account>();
        List<Account> GoogleAccounts = new List<Account>();

        List<Identity> Identities = new List<Identity>();
        List<DBGACLink> GACLinks = new List<DBGACLink>();

        public Dictionary<Account, Identity> Links = new Dictionary<Account, Identity>();
        private SqlConnection DB;



        public GACTool()
        {

        }

        public void StartMatchProccess()
        {
            try
            {
                Status("Start Match");

                GetGoogle();
                GetChris();
                GetAD();
                GetNeuron();

                CheckForDuplicatedEmailsUsed(ChrisAccounts);
                CheckForDuplicatedEmailsUsed(ADAccounts);

                MatchAccountsThenCreate(ChrisAccounts, 0, true);
                MatchAccountsThenCreate(ADAccounts);
                MatchAccountsThenCreate(GoogleAccounts);

                Status("End Match");
            }
            catch (Exception ex)
            {
                Status($"An Error has occured during the sync proccess: {ex.Message}", true);
                Status(ex.StackTrace.ToString(), true);
            }
        }

        //Util
        Identity AddIdenetity(Account A)
        {
            Identity I = new Identity(A, this.Links);
            Identities.Add(I);
            AddLink(I, A, true);
            return I;
        }

        public void AddLink(Identity I, Account A, bool Linked = false)
        {
            I.AddLink(A);
            if (Linked) A.Linked = true;
        }

        public void RemoveIdentity(Identity I)
        {
            foreach (Account A in I.FindLinks())
                RemoveAccount(A);
            Identities.Remove(I);
        }

        public void RemoveAccount(Account A)
        {
            if (A.Linked) A.Linked = false;
            Links.Remove(A);
        }

        T RemoveFromList<T>(List<T> list, int index)
        {
            T item = list.ElementAt(index);
            list.Remove(item);
            return item;
        }


        //Get Data
        void GetGoogle()
        {

            GACDataTableAdapters.GoogleUserTableAdapter G = new GACDataTableAdapters.GoogleUserTableAdapter();
            GACData.GoogleUserDataTable GTable = G.GetData();
            foreach (GACData.GoogleUserRow r in GTable)
            {
                GoogleAccounts.Add(new Account(r));
            }
        }

        void GetChris()
        {
            GACDataTableAdapters.Chris21UserTableAdapter C = new GACDataTableAdapters.Chris21UserTableAdapter();
            GACData.Chris21UserDataTable CTable = C.GetData();
            foreach (GACData.Chris21UserRow r in CTable)
            {
                ChrisAccounts.Add(new Account(r));
            }
        }

        void GetAD()
        {
            GACDataTableAdapters.ADUserTableAdapter ADT = new GACDataTableAdapters.ADUserTableAdapter();
            GACData.ADUserDataTable DT = ADT.GetData();
            foreach (GACData.ADUserRow r in DT)
            {
                ADAccounts.Add(new Account(r));
            }
        }

        void GetNeuron()
        {
            GACDataTableAdapters.EmployeeIntegrationTableAdapter E = new GACDataTableAdapters.EmployeeIntegrationTableAdapter();
            GACData.EmployeeIntegrationDataTable DT = E.GetData();

            foreach (GACData.EmployeeIntegrationRow R in DT)
            {
                int ID = R.EmployeeID.ToString().GetHashCode();
                Account P = ChrisAccounts.Find(x => x.ID.GetHashCode() == ID);
                if (P != null) P.AddSNPEmail(R.EmailAddress);
            }
        }


        // Main Match Logic
        void CheckForDuplicatedEmailsUsed(List<Account> list)
        {
            foreach (Account A in list)
            {
                HashSet<string> Emails = new HashSet<string>(A.SNPEmail.ToList());
                foreach (string E in Emails)
                {
                    List<Account> Matches = list.FindAll(x => x.SNPEmail.Contains(E));
                    if (Matches.Count > 1)
                    {
                        Account G = GoogleAccounts.Find(x => x.SNPEmail.Contains(E));
                        if (G != null)
                            DealWithDuplicatedEmailsUsedInAccounts(Matches, G, E);
                        else Matches.ForEach(x =>
                        {
                            x.SNPEmail.Remove(E);
                        });
                    }
                }
            }
        }

        void DealWithDuplicatedEmailsUsedInAccounts(List<Account> list, Account google, string email)
        {
            //Get identities that are simular to this account
            List<MatchResult<Account>> Results = list.Select(x => x.MatchAccount(google, 1, true).Scale()).ToList();
            // Order Acording To FirstName and LastName then All other Prams(Termination Date + GivenName2)
            Results = Results.OrderBy(x => x.Total_FL).ThenBy(x => x.Total).ToList();
            //Get only results that are equal to the top result
            List<MatchResult<Account>> ValidMatches = Results.FindAll(x => x.Total_FL == Results[0].Total_FL);

            Account SelectedAccount = null;
            // if only one match then select it
            if (ValidMatches.Count == 1) SelectedAccount = RemoveFromList(Results, Results.IndexOf(ValidMatches[0])).I1;
            // if 2 matches then determin the best by the date terminated
            else if (ValidMatches.Count == 2)
            {
                int DateResult = MatchResult.ComparerDates(ValidMatches[0].I1.TerminatedDate, ValidMatches[1].I1.TerminatedDate);
                if (DateResult == 0 || DateResult == 1) SelectedAccount = RemoveFromList(Results, Results.IndexOf(ValidMatches[0])).I1;
                else if (DateResult == 2) SelectedAccount = RemoveFromList(Results, Results.IndexOf(ValidMatches[1])).I1;
                else if (DateResult == -1)
                {
                    Status("Logic not implemented in: DealWithDuplicatedEmailsUsedInAccounts()");
                    Status(String.Format("Could not determ which choice is best.\n{0}\n{1}", ValidMatches[0], ValidMatches[1]));
                }
            }
            else
            {
                Status("Logic not implemented in: DealWithDuplicatedEmailsUsedInAccounts()");
                Status(String.Format("To Many matches:\n{0}\n{1}\n{2}{0}\n{1}\n{2}", ValidMatches[0], ValidMatches[1], ValidMatches[3]));
            }

            //debug
            if ((StatusLevel == 2) || (StatusLevel == 1 && SelectedAccount == null))
            {
                string DebugText = "Duplicated Email Usage";
                if (SelectedAccount != null) DebugText += String.Format("{0}{1}: ({2})*", Environment.NewLine, SelectedAccount.type.ToString(), SelectedAccount.ToString());
                else DebugText += " - Alert No Account Assigned";
                foreach (MatchResult<Account> R in Results)
                {
                    DebugText += String.Format("{0}{1}: ({2})", Environment.NewLine, R.I1.type.ToString(), R.I1.ToString());
                }
                DebugText += String.Format("{0}Google: ({1})", Environment.NewLine, google.ToString());
                Status(DebugText);
            }


            //remove emails from all users that wernt selected
            foreach (MatchResult<Account> R in Results)
            {
                R.I1.SNPEmail.Remove(email);
            }
        }

        void MatchAccountsThenCreate(List<Account> list, int MatchLevel = 1, bool CheckDiffGivenName2 = false)
        {
            foreach (Account A in list)
            {
                if (!MatchAccountToIdentityViaEmail(A))
                    if (!MatchAccountToIdentityViaName(A, MatchLevel, CheckDiffGivenName2))
                        AddIdenetity(A);
            }
        }

        bool MatchAccountToIdentityViaEmail(Account account)
        {
            bool Match = false;
            foreach (string E in account.SNPEmail)
            {
                //Fine identities containing the email address
                List<Identity> Matches = Identities.FindAll(x => x.SNPEmail.Contains(E));
                //If only one match then link
                if (Matches.Count == 1)
                {
                    Identity identity = Matches.First();

                    MatchResult<Identity> R = identity.MatchIdentity(account, 3);
                    if (!R.Match && StatusLevel == 2)
                        Status(
                            String.Format(
                            "Email Mismatch From Name:{0}{4}:({1}){0}{2}:({3})"
                            , Environment.NewLine, identity.ToString(), account.type.ToString(), account.ToString(), identity.FindFirstLink().type.ToString()
                            )
                        );
                    //Link
                    AddLink(identity, account, true);
                    Match = true;
                }
                else if (Matches.Count > 1)
                {
                    Status("Logic not implemented in: MatchAccountToIdentityViaEmail()");
                }

            }
            return Match;
        }

        bool MatchAccountToIdentityViaName(Account account, int level, bool CheckDiffGivenName2)
        {
            bool Match = false;

            List<Identity> Matches = Identities.FindAll(x => x.MatchIdentity(account, level, false, CheckDiffGivenName2).Match);
            if (Matches.Count > 0)
            {
                // List of Match Results with termination date and names scaled
                List<MatchResult<Identity>> Results = Matches.Select(x => x.MatchIdentity(account, 1, true).Scale()).ToList();
                // Order Acording To FirstName and LastName then All other Prams(Termination Date + GivenName2)
                Results = Results.OrderBy(x => x.Total_FL).ThenBy(x => x.Total).ToList();
                // Limit the Results to the identities with the best first name, last name match
                Results = Results.FindAll(x => x.Total_FL == Results[0].Total_FL);

                Identity SelectedAccount = null;
                // if only one match then select it
                if (Results.Count == 1) SelectedAccount = Results[0].I1;
                // if 2 matches then determin the best by the date terminated
                else if (Results.Count == 2)
                {
                    int DateResult = MatchResult.ComparerDates(Results[0].I1.TerminatedDate, Results[1].I1.TerminatedDate);
                    if (DateResult == 0 || DateResult == 1) SelectedAccount = Results[0].I1;
                    else if (DateResult == 2) SelectedAccount = Results[1].I1;
                }
                else
                {
                    Status("Logic not implemented in: MatchAccountToIdentityViaName()");
                    Status(
                        String.Format("To Many matches:{0}Account:{0}{1}{0}Identities:{0}", Environment.NewLine, account)
                        +
                        String.Join(Environment.NewLine, Results.Select(x => x.ToString()))
                        );
                }


                if (SelectedAccount != null)
                {
                    //Link
                    AddLink(SelectedAccount, account, true);
                    Match = true;
                }
            }
            return Match;
        }


        // Write Data

        public void WriteIdentityData()
        {
            Status("Start DB Write");
            cleanIdentity();

            InitiateDBConnection();
            OpenDB();

            GenerateLinks();
            DataTable data = ToDataTable<DBIdentity>(Identities.ToList<DBIdentity>());
            BulkCopy(data, "[Identity]");
            data = ToDataTable<DBGACLink>(GACLinks);
            BulkCopy(data, "GACLink");

            Status("End DB Write");
        }

        void cleanIdentity()
        {
            new GACDataTableAdapters.QueriesTableAdapter().ClearIdentity();
        }

        public void GenerateLinks()
        {
            int ID = 1;
            foreach (Identity I in Identities)
            {
                I.ID = ID++;
            }

            ID = 1;
            foreach (KeyValuePair<Account, Identity> L in Links)
            {
                GACLinks.Add(new DBGACLink()
                {
                    ID = ID++,
                    IdentityID = L.Value.ID,
                    ForeignID = L.Key.ID,
                    ForeignType = (byte)L.Key.type
                });
            }
        }

        private void BulkCopy(DataTable data, string table)
        {
            SqlBulkCopy bulkCopy = new SqlBulkCopy(DB);
            bulkCopy.DestinationTableName = table;
            foreach (DataColumn col in data.Columns)
                bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
            bulkCopy.WriteToServer(data);
        }

        private DataTable ToDataTable<T>(List<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                setPropertyValue(properties, "InsertTime", item, DateTime.Now);
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        private void setPropertyValue(PropertyDescriptorCollection properties, string property, object item, object value)
        {
            if (properties.Contains(properties.Find(property, false)))
            {
                properties.Find(property, false).SetValue(item, value);
            }
        }

        private void InitiateDBConnection()
        {
            string connectionString = Properties.Settings.Default.GACConnectionString;
            DB = new SqlConnection(connectionString);
        }

        private void OpenDB()
        {
            DB.Open();
        }

        private void CloseDB()
        {
            DB.Close();
        }


        //Status
        private void Status(string Status, bool error = false)
        {
            System.Diagnostics.Debug.Print(Environment.NewLine + Status);
            StatusEvent("GAC Sync", Status, error);
        }

    }
}


namespace GAC.GAC
{

    public class DBIdentity
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PreferedFirstName { get; set; }
        public string GivenName2 { get; set; }
        public string GivenName3 { get; set; }
        public DateTime InsertTime { get; set; }

        public DBIdentity()
        {

        }
    }

    public class BasicIdentity : DBIdentity
    {
        public string FirstNameL;
        public string LastNameL;
        public string PreferedFirstNameL;
        public string GivenName2L;
        public string GivenName3L;
        public string UserName;
        public DateTime? TerminatedDate = null;
        public HashSet<string> SNPEmail = new HashSet<string>();


        public BasicIdentity()
        {

        }

        public void Copy(BasicIdentity BI)
        {
            FirstName = BI.FirstName;
            LastName = BI.LastName;
            PreferedFirstName = BI.PreferedFirstName;
            GivenName2 = BI.GivenName2;
            GivenName3 = BI.GivenName3;
            ToLower();

            TerminatedDate = BI.TerminatedDate;
        }

        public void ToLower()
        {
            FirstName = String.IsNullOrEmpty(FirstName) ? "" : FirstName;
            LastName = String.IsNullOrEmpty(LastName) ? "" : LastName;
            PreferedFirstName = String.IsNullOrEmpty(PreferedFirstName) ? "" : PreferedFirstName;
            GivenName2 = String.IsNullOrEmpty(GivenName2) ? "" : GivenName2;
            GivenName3 = String.IsNullOrEmpty(GivenName3) ? "" : GivenName3;

            FirstNameL = String.IsNullOrEmpty(FirstName) ? "" : FirstName.ToLower();
            LastNameL = String.IsNullOrEmpty(LastName) ? "" : LastName.ToLower();
            PreferedFirstNameL = String.IsNullOrEmpty(PreferedFirstName) ? "" : PreferedFirstName.ToLower();
            GivenName2L = String.IsNullOrEmpty(GivenName2) ? "" : GivenName2.ToLower();
            GivenName3L = String.IsNullOrEmpty(GivenName3) ? "" : GivenName3.ToLower();
            UserName = FirstNameL[0] + LastNameL;
        }


    }

    public class MatchResult
    {
        public static int ComparerDates(DateTime? d1, DateTime? d2)
        {
            if (d1.HasValue && d2.HasValue)
            {
                if (d1 == d2) return 0;
                else if (d1 > d2) return 1;
                else if (d1 < d2) return 2;
            }
            else if (!d1.HasValue && d2.HasValue) return 1;
            else if (d1.HasValue && !d2.HasValue) return 2;
            return -1;
        }
    }

    public class MatchResult<T> where T : BasicIdentity
    {
        public int FirstName = 0;
        public int LastName = 0;
        public int MiddleName = 0;
        public int TerminationDate = 0;
        public int Level;
        public int Total { get { return (FirstName + LastName + MiddleName + TerminationDate); } }
        public int Total_FL { get { return (FirstName + LastName); } }
        public int Total_FLM { get { return (FirstName + LastName + MiddleName); } }
        public int Total_FLD { get { return (FirstName + LastName + TerminationDate); } }

        public bool Match { get { return (Total <= Level); } }

        public T I1;
        public BasicIdentity I2;



        public MatchResult(T I1, BasicIdentity I2, int MatchLevel, bool IncludeTerminationDate, bool CheckDiffGivenName2)
        {
            this.I1 = I1;
            this.I2 = I2;

            this.Level = MatchLevel;
            this.FirstName = MatchFirstName();
            this.LastName = MatchLastName();
            this.MiddleName = MatchMiddleName(CheckDiffGivenName2);
            if (IncludeTerminationDate) CalculateTerminationDate();
        }

        public int MatchLastName()
        {
            if (I1.LastNameL != "" && I2.LastNameL != "")
            {
                if (I1.LastNameL.Equals(I2.LastNameL))
                    return 0;
                else
                {
                    int val = 0;
                    val = EditDistance(I1.LastNameL, I2.LastNameL);
                    return val;
                }
            }
            else return 0;
        }

        public int MatchFirstName()
        {
            if (I1.FirstNameL != "" && I2.FirstNameL != "")
            {
                if (I1.FirstNameL.Equals(I2.FirstNameL)
                    || I1.PreferedFirstNameL.Equals(I2.FirstNameL)
                    || I1.FirstNameL.Equals(I2.PreferedFirstNameL))
                    return 0;
                else
                {
                    int val = 0;
                    val = EditDistance(I1.FirstNameL, I2.FirstNameL);
                    return val;
                }
            }
            else return 0;
        }

        public int MatchMiddleName(bool CheckDiffGivenName2)
        {
            if (I1.GivenName2L != "" && I2.GivenName2L != "")
            {
                if (I1.GivenName2L.Equals(I2.GivenName2L))
                    return 0;
                else
                {
                    int val = 0;
                    val = EditDistance(I1.GivenName2L, I2.GivenName2L);
                    return val;
                }
            }
            else if (CheckDiffGivenName2 && (I1.GivenName2L != "" || I2.GivenName2L != ""))
                return Level + 1;
            else return 0;
        }

        public void CalculateTerminationDate()
        {
            if (I1.TerminatedDate.HasValue)
                if (I1.TerminatedDate < DateTime.Now)
                    TerminationDate = Level + 1;
        }

        public MatchResult<T> Scale()
        {
            FirstName *= 1;
            LastName *= 2;
            return this;
        }

        public override string ToString()
        {
            return I1.ToString();
        }



        public int EditDistance(String s1, String s2)
        {
            if (s1.Equals(s2))
            {
                return 0;
            }

            if (s1.Length == 0)
            {
                return s2.Length;
            }

            if (s2.Length == 0)
            {
                return s1.Length;
            }

            // create two work vectors of integer distances
            int[] v0 = new int[s2.Length + 1];
            int[] v1 = new int[s2.Length + 1];
            int[] vtemp;

            // initialize v0 (the previous row of distances)
            // this row is A[0][i]: edit distance for an empty s
            // the distance is just the number of characters to delete from t
            for (int i = 0; i < v0.Length; i++)
            {
                v0[i] = i;
            }

            for (int i = 0; i < s1.Length; i++)
            {
                // calculate v1 (current row distances) from the previous row v0
                // first element of v1 is A[i+1][0]
                //   edit distance is delete (i+1) chars from s to match empty t
                v1[0] = i + 1;

                // use formula to fill in the rest of the row
                for (int j = 0; j < s2.Length; j++)
                {
                    int cost = (s1[i] == s2[j]) ? 0 : 1;
                    v1[j + 1] = Math.Min(
                            v1[j] + 1,              // Cost of insertion
                            Math.Min(
                                    v0[j + 1] + 1,  // Cost of remove
                                    v0[j] + cost)); // Cost of substitution
                }

                // copy v1 (current row) to v0 (previous row) for next iteration
                //System.arraycopy(v1, 0, v0, 0, v0.length);

                // Flip references to current and previous row
                vtemp = v0;
                v0 = v1;
                v1 = vtemp;

            }

            return v0[s2.Length];
        }

    }

    public class Identity : BasicIdentity
    {
        public new HashSet<string> SNPEmail = new HashSet<string>();
        private Dictionary<Account, Identity> GAC_Links;

        public Identity(Account A, Dictionary<Account, Identity> Links)
        {
            Copy(A);
            GAC_Links = Links;
        }

        public List<Account> FindLinks()
        {
            return GAC_Links.Where(x => x.Value == this).Select(x => x.Key).ToList();
        }

        public Account FindFirstLink()
        {
            return FindLinks().First();
        }

        public List<Account> FindChrisLinks()
        {
            return FindLinks().FindAll(x => x.type == PersonType.Chris21);
        }

        public List<Account> FindADLinks()
        {
            return FindLinks().FindAll(x => x.type == PersonType.AD);
        }

        public List<Account> FindGoogleLinks()
        {
            return FindLinks().FindAll(x => x.type == PersonType.Google);
        }

        public void AddLink(Account A)
        {
            GAC_Links.Add(A, this);
            UpdateEmails();
        }

        public void UpdateEmails()
        {
            foreach (Account P in FindLinks())
                foreach (string s in P.SNPEmail)
                    SNPEmail.Add(s);
        }

        public MatchResult<Identity> MatchIdentity(BasicIdentity I2, int MatchLevel = 1, bool IncludeTerminationDate = false, bool CheckDiffGivenName2 = false)
        {
            return new MatchResult<Identity>(
                this,
                I2,
                MatchLevel,
                IncludeTerminationDate,
                CheckDiffGivenName2
                );
        }

        public override string ToString()
        {
            return string.Format("FirstName: {0}, LastName: {1}, EmailAddress: {2}, ChrisID: {3}, DC: {4}, DT: {5}",
                FirstName, LastName, SNPEmail.FirstOrDefault(), FindFirstLink().ID, FindFirstLink().CreatedDate, FindFirstLink().TerminatedDate);
        }
    }

    public enum PersonType { Chris21 = 1, AD = 2, Google = 3 }
    public class Account : BasicIdentity
    {
        public PersonType type;
        public new string ID;
        public new HashSet<string> SNPEmail = new HashSet<string>();
        public DateTime? CreatedDate;
        public bool Linked = false;
        public int ChrisStaff;

        public Account(GACData.GoogleUserRow g)
        {
            type = PersonType.Google;
            ID = g.GoogleUserID;
            FirstName = g.FirstName;
            LastName = g.LastName;
            SNPEmail.Add(g.Email.ToLower());
            CreatedDate = g.CreationDate != null ? DateTime.Parse(g.CreationDate) : new Nullable<DateTime>();
            TerminatedDate = null;

            ToLower();
        }

        public Account(GACData.Chris21UserRow c)
        {
            type = PersonType.Chris21;
            ID = c.ID.ToString();

            FirstName = c.FirstName;
            LastName = c.LastName;
            PreferedFirstName = c.PreferedFirstName;
            GivenName2 = c.GivenName2;
            GivenName3 = c.GivenName3;

            ChrisStaff = c.IsisStaffNull() ? -1 : Convert.ToInt16(c.isStaff);
            AddSNPEmail(c.Email1.ToLower());
            AddSNPEmail(c.Email2.ToLower());
            CreatedDate = c.JoinedDate != null ? DateTime.Parse(c.JoinedDate) : new Nullable<DateTime>();
            TerminatedDate = c.TerminatedDate != null ? DateTime.Parse(c.TerminatedDate) : new Nullable<DateTime>();

            ToLower();
        }

        public Account(GACData.ADUserRow a)
        {
            type = PersonType.AD;
            ID = a.LogonName;
            FirstName = a.FirstName;
            LastName = a.LastName;
            AddSNPEmail(a.Email);
            CreatedDate = a.CreatedDate;

            ToLower();
        }

        public void AddSNPEmail(string e)
        {
            if (e != null)
                e = CheckMultiValueEmailString(e);
            if (e != null) SNPEmail.Add(e);
        }

        string CheckMultiValueEmailString(string e)
        {
            e = e.ToLower();
            string[] strings = { "" };
            if (e.Contains(','))
            {
                strings = e.Split(',');
            }
            else if (e.Contains(';'))
            {
                strings = e.Split(';');
            }
            else
            {
                strings = new String[] { e };
            }
            foreach (string s in strings)
            {
                if (s.Contains("@snpsecurity.com.au"))
                {
                    if (!s.EndsWith("@snpsecurity.com.au"))
                    {
                        return s.Substring(0, s.IndexOf(".au") + 3);
                    }
                    return s;
                }
                else if (s.Contains("@newcastlesecurity.com.au"))
                {
                    if (!s.EndsWith("@newcastlesecurity.com.au"))
                    {
                        return s.Substring(0, s.IndexOf(".au") + 3);
                    }
                    return s;
                }
            }
            return null;
        }

        public MatchResult<Account> MatchAccount(Account I2, int MatchLevel = 1, bool IncludeTerminationDate = false, bool CheckDiffGivenName2 = false)
        {
            return new MatchResult<Account>(
                this,
                I2,
                MatchLevel,
                IncludeTerminationDate,
                CheckDiffGivenName2
                );
        }

        public override string ToString()
        {
            return string.Format("FirstName: {0}, LastName: {1}, EmailAddress: {2}, ID: {3}, Type: {4}, DC: {5}, DT: {6}",
                FirstName, LastName, SNPEmail.FirstOrDefault(), ID, type, CreatedDate, TerminatedDate);
        }

    }

    public class DBGACLink
    {
        public int ID { get; set; }
        public int IdentityID { get; set; }
        public string ForeignID { get; set; }
        public int ForeignType { get; set; }
        public DateTime InsertTime { get; set; }
    }
}