﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using System.Net;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using Google.Apis.Admin.Directory.directory_v1;
using Google.Apis.Admin.Directory.directory_v1.Data;
using Google.Apis.Admin.Reports.reports_v1;
using Google.Apis.Admin.Reports.reports_v1.Data;
using Google.Apis.Licensing.v1;
using Google.Apis.Licensing.v1.Data;
using Google.Apis.Services;
using System.Threading;
using System.Collections.Concurrent;

namespace GAC.GoogleSync
{
    public static class GoogleAPI
    {
        private static DirectoryService dirService = getDirectoryServiceFromLocation();
        private static ReportsService repSevice = getReportingServiceFromLocation();
        private static LicensingService licenseService = getLicensingSerivceFromLocation();
        public static int MaxResults = 500;
        public static string nullDateString = "1970-01-01T00:00:00.000Z";
        public static string[] Domains = { "snpsecurity.com.au", "newcastlesecurity.com.au", "psicorporate.com.au", "psi-corporate.com.au" };

        // Gets all Uers in each domain
        public static GoogleUsers GetAllUsers()
        {
            GoogleUsers Users = new GoogleUsers();
            foreach (string Domain in Domains) GetAllUsersPerDomain(Users, Domain);
            return Users;
        }

        // Gets all users per domain
        public static void GetAllUsersPerDomain(GoogleUsers Users, string Domain)
        {
            UsersResource.ListRequest Request = new UsersResource.ListRequest(dirService);
            Request.Domain = Domain;
            Request.MaxResults = MaxResults;
            Users results = new Users();
            do
            {
                if (results.NextPageToken != null)
                {
                    Request.PageToken = results.NextPageToken;
                }

                results = Request.Execute();
                getUsersPerPage(results.UsersValue, Users);

            } while (results.NextPageToken != null);
        }

        // Proccesses users per page and exstracts info that is needed and restructures the user object
        private static void getUsersPerPage(IList<User> results, GoogleUsers Users)
        {
            if (results != null)
            {
                foreach (User User in results)
                {
                    GoogleUser GUser = new GoogleUser()
                    {
                        FirstName = User.Name.GivenName,
                        LastName = User.Name.FamilyName,
                        GoogleUserID = User.Id,
                        Email = User.PrimaryEmail,
                        CreationDate = CheckDate(User.CreationTimeRaw),
                        LastLoginDate = CheckDate(User.LastLoginTimeRaw),
                        Admin = Convert.ToBoolean(User.IsAdmin),
                        Organization = User.OrgUnitPath,
                    };
                    Users.Add(GUser);
                }
            }
        }

        // Gets Gmail info 
        public static void GetAllUserGmailData(GoogleUsers Users)
        {
            string date = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd"); // Google data onle Available from 3 days ago
            UserUsageReportResource resource = new UserUsageReportResource(repSevice);
            UserUsageReportResource.GetRequest request = resource.Get("all", date);
            request.MaxResults = MaxResults;
            UsageReports results = new UsageReports();

            do
            {
                if (results.NextPageToken != null)
                {
                    request.PageToken = results.NextPageToken;
                }
                results = request.Execute();
                GetUserGmailDataPerPage((List<UsageReport>)results.UsageReportsValue, Users);

            } while (results.NextPageToken != null);

        }

        // Matches Gmail info to the user in the list of users 
        private static void GetUserGmailDataPerPage(List<UsageReport> results, GoogleUsers Users)
        {
            foreach (UsageReport report in results)
            {
                GoogleUser User = Users.Find(report.Entity.ProfileId);

                if (User != null)
                {
                    foreach (UsageReport.ParametersData pram in report.Parameters)
                    {
                        switch (pram.Name)
                        {
                            case "accounts:is_suspended":
                                User.AccountSuspended = Convert.ToBoolean(pram.BoolValue);
                                break;
                            case "accounts:drive_used_quota_in_mb":
                                User.UsedDriveSize = Convert.ToInt32(pram.IntValue);
                                break;
                            case "accounts:gmail_used_quota_in_mb":
                                User.UsedGmailSize = Convert.ToInt32(pram.IntValue);
                                break;
                            case "accounts:used_quota_in_mb":
                                User.UsedTotalSize = Convert.ToInt32(pram.IntValue);
                                break;
                            case "accounts:last_sso_time":
                                User.LastSSODate = CheckDate(pram.DatetimeValueRaw);
                                break;
                            case "gmail:is_gmail_enabled":
                                User.GmailSuspended = !Convert.ToBoolean(pram.BoolValue);
                                break;
                            case "gmail:last_access_time":
                                User.LastGmailLoginDate = CheckDate(pram.DatetimeValueRaw);
                                break;
                            case "gmail:last_imap_time":
                                User.LastIMAPUsedDate = CheckDate(pram.DatetimeValueRaw);
                                break;
                            case "gmail:last_pop_time":
                                User.LastPOPUsedDate = CheckDate(pram.DatetimeValueRaw);
                                break;
                            case "gmail:last_webmail_time":
                                User.LastWEBUsedDate = CheckDate(pram.DatetimeValueRaw);
                                break;
                            case "gmail:last_interaction_time":
                                User.LastInteractionDate = CheckDate(pram.DatetimeValueRaw);
                                break;
                            case "gmail:num_emails_sent":
                                User.TotalSentEmails = Convert.ToInt32(pram.IntValue);
                                break;
                            case "gmail:num_emails_received":
                                User.TotalReceivedEmails = Convert.ToInt32(pram.IntValue);
                                break;
                            case "gmail:num_spam_emails_received":
                                User.TotoalReceivedSpamEmails = Convert.ToInt32(pram.IntValue);
                                break;
                        }
                    }
                }

            }

        }

        // Gets all the groups in each domain
        public static GoogleGroups GetAllDomainGroups()
        {
            GoogleGroups Groups = new GoogleGroups();
            foreach (string Domain in Domains) GetAllDomainGroupsPerDomain(Groups, Domain);
            return Groups;
        }

        // Gets all groups per domian
        private static void GetAllDomainGroupsPerDomain(GoogleGroups Groups, string Domain)
        {
            GroupsResource.ListRequest Request = new GroupsResource.ListRequest(dirService);
            Request.Domain = Domain;
            Request.MaxResults = MaxResults;
            Groups results = new Groups();
            do
            {
                if (results.NextPageToken != null)
                {
                    Request.PageToken = results.NextPageToken;
                }
                results = Request.Execute();
                GetDomainGroupsPerPage((List<Group>)results.GroupsValue, Groups);

            } while (results.NextPageToken != null);
        }

        // Deals with a list of groups and proccesses each group
        private static void GetDomainGroupsPerPage(List<Group> results, GoogleGroups Groups)
        {
            if (results != null)
            {
                foreach (Group Group in results)
                {
                    Groups.Add(new GoogleGroup()
                    {
                        Name = Group.Name,
                        Email = Group.Email,
                        Description = CheckDescription(Group.Description),
                        GroupID = Group.Id,
                        MemberCount = Convert.ToInt32(Group.DirectMembersCount)
                    });
                }
            }
        }

        // Get all the users in a list of groups
        public static GroupUserLinks GetAllUsersInGroups(GoogleGroups Groups, GoogleUsers Users)
        {
            GroupUserLinks Links = new GroupUserLinks();

            List<Task> Tasks = new List<Task>();

            int count = 0;

            foreach (GoogleGroup Group in Groups.Groups.Values)
            {
                count += 1;
                Tasks.Add(Task.Run(() => GetUsersInGroup(Group, Links, Users)));

                if (count >= 20)
                {
                    Task.WaitAll(Tasks.ToArray());
                    Tasks.Clear();
                    Thread.Sleep(1000);
                    count = 0;
                }

            }
            Task.WaitAll(Tasks.ToArray());
            return Links;
        }

        // Gets all the users in a single group
        private static async Task GetUsersInGroup(GoogleGroup Group, GroupUserLinks Links, GoogleUsers Users)
        {
            MembersResource.ListRequest Request = new MembersResource.ListRequest(dirService, Group.GroupID);
            Request.MaxResults = MaxResults;
            Members results = new Members();

            do
            {
                if (results.NextPageToken != null)
                {
                    Request.PageToken = results.NextPageToken;
                }

                results = Request.Execute();

                if (results.MembersValue != null)
                {
                    GetUsersInGroupPerPage((List<Member>)results.MembersValue, Group, Links, Users);
                }

            } while (results.NextPageToken != null);

        }

        // Proccesses the users in a group and makes the links
        private static void GetUsersInGroupPerPage(List<Member> results, GoogleGroup Group, GroupUserLinks Links, GoogleUsers Users)
        {
            foreach (Member m in results)
            {
                GroupUserLink link = new GroupUserLink()
                {
                    GoogleUserID = m.Id,
                    GoogleGroupID = Group.GroupID
                };
                if (!Users.Exists(link.GoogleUserID))
                {
                    link.isSNP = false;
                    link.Email = m.Email;
                }
                Links.add(link);
            }
        }

        // Gets all the licences for every usere for each product
        public static Licenses GetAllLicenceProducts()
        {
            Licenses Licences = new Licenses();
            List<Task> Tasks = new List<Task>();

            Tasks.Add(Task.Run(() => GetUsersLicencePerProduct("Google-Apps", Licences)));
            Tasks.Add(Task.Run(() => GetUsersLicencePerProduct("Google-Drive-storage", Licences)));
            Tasks.Add(Task.Run(() => GetUsersLicencePerProduct("Google-Vault", Licences)));

            Task.WaitAll(Tasks.ToArray());
            return Licences;
        }

        // Handles getting licences per product
        private static async Task GetUsersLicencePerProduct(string product, Licenses licenceList)
        {
            LicenseAssignmentsResource LAR = new LicenseAssignmentsResource(licenseService);
            LicenseAssignmentsResource.ListForProductRequest Request = LAR.ListForProduct(product, "snpsecurity.com.au");
            Request.MaxResults = MaxResults;
            LicenseAssignmentList Results = new LicenseAssignmentList();

            await Task.Run(() =>
            {
                do
                {
                    if (Results.NextPageToken != null)
                    {
                        Request.PageToken = Results.NextPageToken;
                    }

                    Results = Request.Execute();
                    GetUsersPerProductPerPage((List<LicenseAssignment>)Results.Items, licenceList);

                } while (Results.NextPageToken != null);
            });
        }

        // Handles proccessing a list of licences and recording the links
        private static void GetUsersPerProductPerPage(List<LicenseAssignment> results, Licenses L)
        {
            foreach (LicenseAssignment LA in results)
            {
                L.add(LA.SkuId, LA.UserId);
            }
        }

        // Gets All Login Activities for the Domain
        public static LoginActivities GetAllLoginActivities()
        {
            LoginActivities loginList = new LoginActivities();
            ActivitiesResource resource = new ActivitiesResource(repSevice);
            ActivitiesResource.ListRequest request = resource.List("all", "login");
            request.MaxResults = MaxResults;
            Activities results = new Activities();

            do
            {
                if (results.NextPageToken != null)
                {
                    request.PageToken = results.NextPageToken;
                }
                results = request.Execute();
                GetLoginsPerPage((List<Activity>)results.Items, loginList);

            } while (results.NextPageToken != null);

            return loginList;
        }

        // Handles processing of Activities per page
        private static void GetLoginsPerPage(List<Activity> list, LoginActivities LoginList)
        {
            foreach (Activity activity in list)
            {
                LoginList.add(
                    activity.Actor.Email,
                    activity.Id.Time.Value,
                    activity.IpAddress,
                    activity.Events.First().Name
                    );
            }
        }



        private static DateTime ConvertDate(string dateString)
        {
            DateTime date = DateTime.Parse(dateString, null, System.Globalization.DateTimeStyles.RoundtripKind);
            date = TimeZoneInfo.ConvertTimeFromUtc(date, TimeZoneInfo.FindSystemTimeZoneById("E. Australia Standard Time"));
            return date;
        }

        private static DateTime? CheckDate(string dateString)
        {
            if (dateString != nullDateString)
                return ConvertDate(dateString);
            else
                return null;
        }

        private static string CheckDescription(string Description)
        {
            if (String.IsNullOrEmpty(Description) || Description == "l")
                return null;
            else
                return Description;
        }


        private static DirectoryService getDirectoryServiceFromLocation()
        {
            String serviceAccountEmail = "458466855031-np5p6n9tbojlh7gbfadrn2vqpnpk7e9i@developer.gserviceaccount.com";

            X509Certificate2 cert = new X509Certificate2(Properties.Resources.Google_API, "notasecret", X509KeyStorageFlags.Exportable);
            //var certificate = @"C:\users\dbard\downloads\Google API-cfb838de355a.p12";

            ServiceAccountCredential credential = new ServiceAccountCredential(
                new ServiceAccountCredential.Initializer(serviceAccountEmail)
                {
                    User = "neuron@snpsecurity.com.au", //the account we will be impersonating. This needs to be an administrator
                    Scopes = new[] {    //The scopes our service will be required to access
                                                    DirectoryService.Scope.AdminDirectoryGroup,
                                                    DirectoryService.Scope.AdminDirectoryUser,
                                                    DirectoryService.Scope.AdminDirectoryGroupMember,
                                                    DirectoryService.Scope.AdminDirectoryOrgunit,
                                                    DirectoryService.Scope.AdminDirectoryUserschema,
                                                    "https://apps-apis.google.com/a/feeds/emailsettings/2.0/"
                                                }
                }.FromCertificate(cert));

            var dirservice = new DirectoryService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "IntegrationServices",
            });

            return dirservice;
        }

        private static ReportsService getReportingServiceFromLocation()
        {
            String serviceAccountEmail = "458466855031-np5p6n9tbojlh7gbfadrn2vqpnpk7e9i@developer.gserviceaccount.com";

            X509Certificate2 cert = new X509Certificate2(Properties.Resources.Google_API, "notasecret", X509KeyStorageFlags.Exportable);

            Google.Apis.Auth.OAuth2.ServiceAccountCredential credential = new Google.Apis.Auth.OAuth2.ServiceAccountCredential(
                new Google.Apis.Auth.OAuth2.ServiceAccountCredential.Initializer(serviceAccountEmail)
                {
                    User = "neuron@snpsecurity.com.au", //the account we will be impersonating. This needs to be an administrator
                    Scopes = new[] {    //The scopes our service will be required to access
                                                   ReportsService.Scope.AdminReportsAuditReadonly,
                                                   ReportsService.Scope.AdminReportsUsageReadonly

                                                }
                }.FromCertificate(cert));

            var rptService = new Google.Apis.Admin.Reports.reports_v1.ReportsService(new Google.Apis.Services.BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "IntegrationServices",
            });

            return rptService;
        }

        private static LicensingService getLicensingSerivceFromLocation()
        {
            String serviceAccountEmail = "458466855031-np5p6n9tbojlh7gbfadrn2vqpnpk7e9i@developer.gserviceaccount.com";

            X509Certificate2 cert = new X509Certificate2(Properties.Resources.Google_API, "notasecret", X509KeyStorageFlags.Exportable);

            ServiceAccountCredential credential = new ServiceAccountCredential(
                 new ServiceAccountCredential.Initializer(serviceAccountEmail)
                 {
                     User = "neuron@snpsecurity.com.au", //the account we will be impersonating. This needs to be an administrator
                     Scopes = new[] {    //The scopes our service will be required to access
                       LicensingService.Scope.AppsLicensing
                   }
                 }.FromCertificate(cert));

            var licService = new LicensingService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "IntegrationServicesLicense",
            });

            return licService;
        }

    }


    // Object classes for information storage



    public class GoogleUsers
    {
        public Dictionary<string, GoogleUser> Users;
        public GoogleUsers()
        {
            Users = new Dictionary<string, GoogleUser>();
        }
        public void Add(GoogleUser User)
        {
            Users.Add(User.GoogleUserID, User);
        }
        public GoogleUser Find(String ID)
        {
            try
            {
                return Users[ID];
            }
            catch (Exception)
            {
                return null;
            }
        }
        public GoogleUser Find(string FirstName, string LastName)
        {
            try
            {
                return Users.FirstOrDefault(x => x.Value.FirstName.Contains(
                    FirstName, StringComparison.OrdinalIgnoreCase)
                    &&
                    x.Value.LastName.Contains(LastName, StringComparison.OrdinalIgnoreCase)
                    ).Value;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool Exists(String ID)
        {
            try
            {
                return Users[ID] != null;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }

    public class GoogleUser
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string GoogleUserID { get; set; }
        public string Email { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public DateTime? LastGmailLoginDate { get; set; }
        public DateTime? LastWEBUsedDate { get; set; }
        public DateTime? LastPOPUsedDate { get; set; }
        public DateTime? LastIMAPUsedDate { get; set; }
        public DateTime? LastInteractionDate { get; set; }
        public DateTime? LastSSODate { get; set; }
        public string Organization { get; set; }
        public bool AccountSuspended { get; set; }
        public bool GmailSuspended { get; set; }
        public bool Admin { get; set; }
        public int UsedDriveSize { get; set; }
        public int UsedGmailSize { get; set; }
        public int UsedTotalSize { get; set; }
        public int TotalSentEmails { get; set; }
        public int TotalReceivedEmails { get; set; }
        public int TotoalReceivedSpamEmails { get; set; }
        public DateTime InsertTime { get; set; }
    }

    public class GoogleGroups
    {
        public Dictionary<string, GoogleGroup> Groups;
        public GoogleGroups()
        {
            Groups = new Dictionary<string, GoogleGroup>();
        }
        public void Add(GoogleGroup Group)
        {
            Groups.Add(Group.GroupID, Group);
        }
    }

    public class GoogleGroup
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string GroupID { get; set; }
        public string Description { get; set; }
        public int MemberCount { get; set; }
        public DateTime InsertTime { get; set; }
    }

    public class GroupUserLinks
    {
        public ConcurrentQueue<GroupUserLink> Links;

        public GroupUserLinks()
        {
            Links = new ConcurrentQueue<GroupUserLink>();
        }

        public void add(GroupUserLink link)
        {
            Links.Enqueue(link);
        }
    }

    public class GroupUserLink
    {
        public int ID { get; set; }
        public string GoogleUserID { get; set; }
        public string GoogleGroupID { get; set; }
        public bool isSNP { get; set; }
        public string Email;
        public DateTime InsertTime { get; set; }

        public GroupUserLink()
        {
            isSNP = true;
        }
    }

    public class NonSNPEmail
    {
        public int ID { get; set; }
        public string GoogleUserID { get; set; }
        public string Email { get; set; }
        public DateTime InsertTime { get; set; }
    }

    public class Licenses
    {
        public ConcurrentQueue<Licence> Licences;

        public Licenses()
        {
            Licences = new ConcurrentQueue<Licence>();
        }

        public void add(string product, string GoogleUserID)
        {
            Licences.Enqueue(new Licence()
            {
                Email = GoogleUserID,
                License = product
            });
        }
    }

    public class Licence
    {
        public int ID { get; set; }
        public string Email { get; set; }
        public string License { get; set; }
        public DateTime InsertTime { get; set; }
    }

    public class LoginActivities
    {
        public List<LoginActivity> activities;

        public LoginActivities()
        {
            activities = new List<LoginActivity>();
        }

        public void add(string email, DateTime date, string ip, string type)
        {
            activities.Add(new LoginActivity()
            {
                Email = email,
                Date = date,
                IP = ip,
                Type = type
            });
        }
    }

    public class LoginActivity
    {
        public int ID { get; set; }
        public string Email { get; set; }
        public DateTime Date { get; set; }
        public string IP { get; set; }
        public string Type { get; set; }
    }

    public static class StringExtensions
    {
        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            if (string.IsNullOrEmpty(toCheck) || string.IsNullOrEmpty(source))
                return false;
            return source.IndexOf(toCheck, comp) >= 0;
        }
    }
}
