﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace GAC.GoogleSync
{
    public delegate void Done();
    public partial class GoogleForm : Form
    {
        public event Done done = delegate { };
        GoogleSync GoogleAudit;

        public GoogleForm()
        {
            InitializeComponent();

            GoogleAudit = new GoogleSync();
            GoogleAudit.StatusEvent += AddText;
            GoogleAudit.done += GoogleAudit_done;
            AddText("Google Form", "Ready");
        }

        void GoogleAudit_done()
        {
            done.Invoke();
        }

        private void StartProccessBTN_Click(object sender, EventArgs e)
        {
            OutputText.Clear();
            Thread t = new Thread(GoogleAudit.GetGooogleData);
            t.IsBackground = true;
            t.Start();
            StartProccessBTN.Enabled = false;
        }

        private void AddText(string Location, string Status, bool error = false)
        {
            if (OutputText.InvokeRequired)
            {
                OutputText.Invoke(new Action(() => { AddText(Location, Status); }));
            }
            else OutputText.AppendText(Status + Environment.NewLine);
        }
    }
}
