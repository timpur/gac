﻿namespace GAC.GoogleSync
{
    partial class GoogleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GoogleForm));
            this.OutputText = new System.Windows.Forms.TextBox();
            this.StartProccessBTN = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // OutputText
            // 
            this.OutputText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OutputText.Location = new System.Drawing.Point(0, 0);
            this.OutputText.Multiline = true;
            this.OutputText.Name = "OutputText";
            this.OutputText.Size = new System.Drawing.Size(284, 225);
            this.OutputText.TabIndex = 3;
            // 
            // StartProccessBTN
            // 
            this.StartProccessBTN.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.StartProccessBTN.Location = new System.Drawing.Point(0, 225);
            this.StartProccessBTN.Name = "StartProccessBTN";
            this.StartProccessBTN.Size = new System.Drawing.Size(284, 37);
            this.StartProccessBTN.TabIndex = 2;
            this.StartProccessBTN.Text = "UpDate Google User Data";
            this.StartProccessBTN.UseVisualStyleBackColor = true;
            this.StartProccessBTN.Click += new System.EventHandler(this.StartProccessBTN_Click);
            // 
            // GoogleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.OutputText);
            this.Controls.Add(this.StartProccessBTN);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GoogleForm";
            this.Text = "GoogleUsers";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox OutputText;
        private System.Windows.Forms.Button StartProccessBTN;
    }
}

