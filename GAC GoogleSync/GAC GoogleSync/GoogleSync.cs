﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel;

namespace GAC.GoogleSync
{
    public delegate void StatusEvent(string Location, string Status, bool error);
    public class GoogleSync
    {
        public event StatusEvent StatusEvent;
        public event Done done = delegate { };

        GoogleUsers Users;
        GoogleGroups Groups;
        GroupUserLinks Links;
        Licenses Licences;
        LoginActivities LoginActivities;
        List<NonSNPEmail> NonSNP = new List<NonSNPEmail>();

        SqlConnection DB;

        public void GetGooogleData()
        {
            try
            {
                Status("Starting Proccess");
                Status("Getting All SNP Security Domain Users");
                GetAllGoogleUsers();
                Status("Getting All SNP Security Domain Groups");
                GetGroupData();
                Status("Getting All License Information");
                GetLicenseInfo();
                Status("Getting All Login Activities");
                GetLoginActivities();

                Status("Initializing DB");
                InitializeGoogleUsersTables();
                Status("Writing All Users");
                InsertAllUsersToDB();
                Status("Writing All Groups");
                InsertAllGroupsToDB();
                Status("Writing All Link Information");
                InsertAllLinksToDB();
                Status("Writing All NON SNP Emails");
                InsertAllNonSNPEmails();
                Status("Writing All License Information");
                InsertAllLicenses();
                Status("Writing All Login Activities");
                InsertAllLoginActivities();
                Status("Finished");
                done.Invoke();
            }
            catch (Exception ex)
            {
                Status($"An Error has occured during the sync proccess: {ex.Message}", true);
                Status(ex.StackTrace.ToString(), true);
            }

        }


        private void GetAllGoogleUsers()
        {
            Status("Getting All Domain Users");
            Users = GoogleAPI.GetAllUsers();
            Status("Getting All User Gmail Data");
            GoogleAPI.GetAllUserGmailData(Users);
        }

        private void GetGroupData()
        {
            Status("Getting All Domain Groups");
            Groups = GoogleAPI.GetAllDomainGroups();
            Status("Getting All Group Members");
            Links = GoogleAPI.GetAllUsersInGroups(Groups, Users);
        }

        private void GetLicenseInfo()
        {
            Licences = GoogleAPI.GetAllLicenceProducts();
        }

        private void GetLoginActivities()
        {
            LoginActivities = GoogleAPI.GetAllLoginActivities();
        }


        private void InitializeGoogleUsersTables()
        {
            InitiateDBConnection();
            OpenDB();

            SqlCommand cmd = new SqlCommand("InitializeGoogleUsersTable", DB);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();

            CloseDB();
        }

        private void InsertAllUsersToDB()
        {
            InitiateDBConnection();
            OpenDB();

            DataTable data = ToDataTable<GoogleUser>(Users.Users.Values.ToList());
            BulkCopy(data, "GoogleUsers");

            CloseDB();
        }

        private void InsertAllGroupsToDB()
        {
            InitiateDBConnection();
            OpenDB();

            DataTable data = ToDataTable<GoogleGroup>(Groups.Groups.Values.ToList());
            BulkCopy(data, "GoogleGroups");

            CloseDB();
        }

        private void InsertAllLinksToDB()
        {
            InitiateDBConnection();
            OpenDB();

            DataTable data = ToDataTable<GroupUserLink>(Links.Links.ToList());
            BulkCopy(data, "GroupUserLink");

            CloseDB();
        }

        private void InsertAllNonSNPEmails()
        {
            List<string> emails = new List<string>();
            foreach (GroupUserLink link in Links.Links)
            {
                if (link != null && !link.isSNP && !emails.Contains(link.GoogleUserID))
                {
                    emails.Add(link.Email);
                    NonSNP.Add(new NonSNPEmail()
                    {
                        GoogleUserID = link.GoogleUserID,
                        Email = link.Email
                    });
                }
            }

            InitiateDBConnection();
            OpenDB();

            DataTable data = ToDataTable<NonSNPEmail>(NonSNP);
            BulkCopy(data, "NonSNPEmails");

            CloseDB();

        }

        private void InsertAllLicenses()
        {
            InitiateDBConnection();
            OpenDB();

            DataTable data = ToDataTable<Licence>(Licences.Licences.ToList());
            BulkCopy(data, "GoogleLicenses");

            CloseDB();
        }

        private void InsertAllLoginActivities()
        {
            InitiateDBConnection();
            OpenDB();

            DataTable data = ToDataTable<LoginActivity>(LoginActivities.activities);
            BulkCopy(data, "GoogleLoginActivities");

            CloseDB();
        }

        private void BulkCopy(DataTable data, string table)
        {
            SqlBulkCopy bulkCopy = new SqlBulkCopy(DB);
            bulkCopy.DestinationTableName = table;
            foreach (DataColumn col in data.Columns)
                bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
            bulkCopy.WriteToServer(data);
        }

        private DataTable ToDataTable<T>(List<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            int ID = 1;
            foreach (T item in data)
            {
                setPropertyValue(properties, "ID", item, ID++);
                setPropertyValue(properties, "InsertTime", item, DateTime.Now);
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        private void setPropertyValue(PropertyDescriptorCollection properties, string property, object item, object value)
        {
            if (properties.Contains(properties.Find(property, false)))
            {
                properties.Find(property, false).SetValue(item, value);
            }
        }

        private void InitiateDBConnection()
        {
            string connectionString = Properties.Settings.Default.GoogleConnectionString;
            DB = new SqlConnection(connectionString);
        }

        private void OpenDB()
        {
            DB.Open();
        }

        private void CloseDB()
        {
            DB.Close();
        }

        private void Status(string Status, bool error = false)
        {
            System.Diagnostics.Debug.Print(Environment.NewLine + Status);
            StatusEvent("Google Sync", Status, error);
        }

    }
}
