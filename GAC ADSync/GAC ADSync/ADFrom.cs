﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GAC.ADSync
{
    public delegate void Done();
    public partial class ADFrom : Form
    {
        public event Done done = delegate { };
        ADSync AD;

        public ADFrom()
        {
            InitializeComponent();
        }

        private void ADForm_Load(object sender, EventArgs e)
        {
            AD = new ADSync();
            AD.StatusEvent += AD_Debug;
            AD.done += AD_done;
        }

        void AD_done()
        {
            done.Invoke();
        }

        void AD_Debug(string Location, string Status, bool error)
        {
            textBox1.Invoke(new Action(() => textBox1.AppendText(Status + Environment.NewLine)));
        }

        private void SyncBTN_Click(object sender, EventArgs e)
        {
            System.Threading.Thread t = new System.Threading.Thread(AD.GetADData);
            t.IsBackground = true;
            t.Start();
            SyncBTN.Enabled = false;
        }
    }
}
