﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.ActiveDirectory;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel;

namespace GAC.ADSync
{
    public delegate void StatusEvent(string Location, string Status, bool error);
    public class ADSync
    {
        public event Done done = delegate { };
        public event StatusEvent StatusEvent;

        List<ADUser> Users = new List<ADUser>();

        public SqlConnection DB;

        public ADSync()
        {

        }

        public void GetADData()
        {
            try
            {
                Status("Start");
                Status("Getting All AD Users");
                GetADUsers();
                Status("Writing All Users to DB");
                WriteADUsers();
                Status("Done");
                done.Invoke();
            }
            catch (Exception ex)
            {
                Status($"An Error has occured during the sync proccess: {ex.Message}", true);
                Status(ex.StackTrace.ToString(), true);
            }

        }

        private void GetADUsers()
        {
            List<UserPrincipal> Results = Search();

            foreach (UserPrincipal Result in Results)
            {
                ADUser user = new ADUser
                {
                    LastName = Result.Surname,
                    FirstName = Result.GivenName,
                    Email = Result.EmailAddress,
                    CreatedDate = DateTime.Parse(GetProperty(Result, "whenCreated")),
                    Disabled = !Result.Enabled.Value,
                    LogonName = Result.SamAccountName.ToLower(),
                    MobilePhone = GetProperty(Result, "mobile"),
                    JobTitle = GetProperty(Result, "title"),
                    Department = GetProperty(Result, "department"),
                    Company = GetProperty(Result, "company"),
                    LastLogon = Result.LastLogon,
                    Organization = Path(Result.DistinguishedName)
                };
                Users.Add(user);
            }
        }

        private void WriteADUsers()
        {
            InitiateDBConnection();
            OpenDB();

            InitializeGACADTable();
            DataTable data = ToDataTable<ADUser>(Users);
            BulkCopy(data, "ADUser");

            CloseDB();
        }

        private void InitializeGACADTable()
        {
            SqlCommand cmd = new SqlCommand("ClearADUserTable", DB);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }

        private void InitiateDBConnection()
        {
            string connectionString = Properties.Settings.Default.GACConnectionString;
            DB = new SqlConnection(connectionString);
        }

        private void OpenDB()
        {
            DB.Open();
        }

        private void CloseDB()
        {
            DB.Close();
        }

        private void BulkCopy(DataTable data, string table)
        {
            SqlBulkCopy bulkCopy = new SqlBulkCopy(DB);
            bulkCopy.DestinationTableName = table;
            foreach (DataColumn col in data.Columns)
                bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
            bulkCopy.WriteToServer(data);
        }

        private DataTable ToDataTable<T>(List<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            int ID = 1;
            foreach (T item in data)
            {
                setPropertyValue(properties, "ID", item, ID++);
                setPropertyValue(properties, "InsertTime", item, DateTime.Now);
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        private void setPropertyValue(PropertyDescriptorCollection properties, string property, object item, object value)
        {
            if (properties.Contains(properties.Find(property, false)))
            {
                properties.Find(property, false).SetValue(item, value);
            }
        }

        private List<UserPrincipal> Search()
        {


            PrincipalContext DomainContext = new PrincipalContext(ContextType.Domain);
            UserPrincipal Filter = new UserPrincipal(DomainContext);
            Filter.Surname = "*";
            Filter.GivenName = "*";
            PrincipalSearcher srch = new PrincipalSearcher(Filter);
            return srch.FindAll().Select(x => x as UserPrincipal).ToList();

        }

        private String GetProperty(Principal principal, String property)
        {
            DirectoryEntry directoryEntry = principal.GetUnderlyingObject() as DirectoryEntry;
            if (directoryEntry.Properties.Contains(property))
                return directoryEntry.Properties[property].Value.ToString();
            else
                return null;
        }

        private string Path(string DN)
        {
            string[] Names = DN.Split(',');
            Names = Names.Reverse().ToArray();

            string path = "";

            foreach (string Name in Names)
            {
                string[] parts = Name.Split('=');
                if (parts[0].ToLower() == "ou")
                {
                    path += "/" + parts[1];
                }
            }

            if (path == "") path = "/";
            return path;
        }

        private void Status(string Status, bool error = false)
        {
            System.Diagnostics.Debug.Print(Environment.NewLine + Status);
            StatusEvent("AD Sync", Status, error);
        }

    }

    public class ADUser
    {
        public int ID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool Disabled { get; set; }
        public string LogonName { get; set; }
        public string MobilePhone { get; set; }
        public string JobTitle { get; set; }
        public string Department { get; set; }
        public string Company { get; set; }
        public DateTime? LastLogon { get; set; }
        public string Organization { get; set; }
        public DateTime InsertTime { get; set; }

        public ADUser()
        {

        }
    }
}
