﻿namespace GAC.ADSync
{
    partial class ADFrom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ADFrom));
            this.SyncBTN = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // SyncBTN
            // 
            this.SyncBTN.Location = new System.Drawing.Point(12, 218);
            this.SyncBTN.Name = "SyncBTN";
            this.SyncBTN.Size = new System.Drawing.Size(260, 32);
            this.SyncBTN.TabIndex = 0;
            this.SyncBTN.Text = "Get And White AD Users";
            this.SyncBTN.UseVisualStyleBackColor = true;
            this.SyncBTN.Click += new System.EventHandler(this.SyncBTN_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 12);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(260, 200);
            this.textBox1.TabIndex = 1;
            // 
            // ADFrom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.SyncBTN);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ADFrom";
            this.Text = "GAC ADSync";
            this.Load += new System.EventHandler(this.ADForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SyncBTN;
        private System.Windows.Forms.TextBox textBox1;
    }
}

